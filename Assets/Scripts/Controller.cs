﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : Singleton<Controller>
{
    
    [HideInInspector]
    public Model model;
    [HideInInspector]
    public View view;

    private AudioManager _audioManager;
    private EventManager _eventManager;
    private GameManager _gameManager;

    private Vector3 _cameraPosition;

    protected override void Awake()
    {
        base.Awake();
        _cameraPosition = Camera.main.transform.position;
        model = GameObject.FindGameObjectWithTag("Model").GetComponent<Model>();
        view = GameObject.FindGameObjectWithTag("View").GetComponent<View>();
        _audioManager = AudioManager.Instance;
        _eventManager = EventManager.Instance;
        _gameManager = GameManager.Instance;
        _eventManager.Add(GameEventType.GAME_START, GameStart);
        _eventManager.Add(GameEventType.GAME_PAUSE, GamePause);
        _eventManager.Add(GameEventType.GAME_OVER, GameOver);
        _eventManager.Add(GameEventType.REFRESH_SCORE, GetScoreInfo);
        _eventManager.Add(GameEventType.CLEAR_DATA, ClearData);
        _eventManager.Add(GameEventType.SET_MUTE, SetMute);
        _eventManager.Add(GameEventType.BRICK_IN_LINE, CheckLine);
        _eventManager.Add(GameEventType.VOLUME_MUSIC, SetMusicVolume);
        _eventManager.Add(GameEventType.VOLUME_SOUND, SetSoundVolume);
    }

    // сброс сохраненных данных
    private void ClearData(object obj = null)
    {
        Debug.Assert(obj != null, nameof(obj) + " != null");
        var panelType = (int)obj;
        model.ClearData();
        view.UpdatePanelInfo(panelType, model.GetScoreInfo());
    }

    // проверка линии на заполненность
    private void CheckLine(object obj)
    {
        if(!(obj is int))
        {
            return;
        }
        model.CheckLine(int.Parse(obj.ToString()));
    }

    // завершаем игру
    private void GameOver(object obj = null)
    {
        view.GameOver();
    }

    // ставим игру на паузу
    private void GamePause(object obj = null)
    {
        Debug.Assert(obj != null, nameof(obj) + " != null");
        view.GamePause();
        var isRestart = (bool)obj;
        if (isRestart)
        {
            AudioManager.Instance.Pause();
            GameManager.Instance.GamePause();
        }
        
    }

    // запускаем игру
    private void GameStart(object obj = null)
    {
        //Debug.Log("Стартуем игровой уровень!");
        Debug.Assert(obj != null, nameof(obj) + " != null");
        var isRestart = (bool)obj;
        if (isRestart)
        {
            model.RefreshGame();
        }
        GameManager.Instance.GameStart();
        AudioManager.Instance.Play();
    }

    // получаем игровые данные (текущие счет, рекорды и т.д.)
    private void GetScoreInfo(object obj = null)
    {
        Debug.Assert(obj != null, nameof(obj) + " != null");
        var panelType = (int)obj;
        view.UpdatePanelInfo(panelType, model.GetScoreInfo());
    }

    // устанавливаем бесшумный режим
    private void SetMute(object obj = null)
    {
        Debug.Assert(obj != null, nameof(obj) + " != null");
        var isMute = (bool)obj;
        AudioManager.Instance.SetMute(isMute);
    }

    // устанваливаем громкость музыки
    private void SetMusicVolume(object obj = null)
    {
        Debug.Assert(obj != null, nameof(obj) + " != null");
        var newVolume = (float)obj;
        AudioManager.Instance.SetMusicVolume(newVolume);
    }

    // устанавливаем громкость звуков
    private void SetSoundVolume(object obj = null)
    {
        Debug.Assert(obj != null, nameof(obj) + " != null");
        var newVolume = (float)obj;
        AudioManager.Instance.SetSoundsVolume(newVolume);
    }

}