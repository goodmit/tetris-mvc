﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Типы действий по перемещению фигуры в пространстве
/// </summary>
public enum Movements
{
    DOWN,
    LEFT,
    RIGHT,
    ROTATE_LEFT,
    ROTATE_RIGHT
}

/// <summary>
/// Класс, описывающий игровую фигуру
/// </summary>
public class Figure : MonoBehaviour
{
    private Transform _pivot;
    private Controller _controller;
    private bool _isTurbo = false;

    void Awake()
    {
        _pivot = transform.Find("Pivot");
        _controller = Controller.Instance;
    }

    /// <summary>
    /// Инициализация фигуры
    /// </summary>
    /// <param name="material">материал фигуры</param>
    /// <param name="initRotationOffset">начальное положение фигуры (разворот)</param>
    public void Init(Material material, int initRotationOffset)
    {
        foreach (Transform brick in transform)
        {
            if (brick.CompareTag("Brick"))
            {
                brick.GetComponent<MeshRenderer>().material = material;
            }
        }

        while (initRotationOffset > 0)
        {
            transform.RotateAround(_pivot.position, Vector3.forward, -90);
            initRotationOffset--;
        }
    }

    /// <summary>
    /// Активирован ли режим Турбо?
    /// </summary>
    public bool IsTurbo()
    {
        return _isTurbo;
    }

    /// <summary>
    /// Перемещаем фигуру на один ряд ниже
    /// </summary>
    public void Fall()
    {
        Vector3 position = transform.position;
        position.y--;
        transform.position = position;
        if (_controller.model.IsCorrectPosition(transform) == false)
        {
            position.y++;
            transform.position = position;
            
            // фиксируем фигуру на поле
            _controller.model.PlaceFigure(transform);
        }
        else
        {
            AudioManager.Instance.PlayDrop();
        }
    }

    /// <summary>
    /// Передвигаем фигуру влево или вправо
    /// </summary>
    /// <param name="reverse">если true, то включаем реверс и двигаем фигуру в левую сторону</param>
    public void Move(bool reverse = false)
    {
        lock (_pivot)
        {
            int step = reverse ? -1 : 1;
            Vector3 newPosition = transform.position;
            newPosition.x += step;
            transform.position = newPosition;
            
            //Если не можем повернуть фигуру
            if (_controller.model.IsCorrectPosition(transform) == false)
            {
                newPosition.x -= step;
                transform.position = newPosition;
            }
            else
            {
                AudioManager.Instance.PlayControl();
            }
        }
    }

    /// <summary>
    /// Поворачиваем фигуру вокруг своей оси
    /// </summary>
    /// <param name="antiClockWise">если true, то поворот фигуры производится рпотив часовой стрелки</param>
    public void Rotate(bool antiClockWise = false)
    {
        int index = antiClockWise ? 1 : -1;
        transform.RotateAround(_pivot.position, Vector3.forward, 90 * index);
        //Если вы не можете повернуть
        if (_controller.model.IsCorrectPosition(transform) == false)
        {
            transform.RotateAround(_pivot.position, Vector3.forward, 90 * (index * -1));
        }
        else
        {
            AudioManager.Instance.PlayControl();
        }
    }

    /// <summary>
    /// Запуск фигуры. Необходим для инициации всех кубиков внутри.
    /// Для того, чтобы не возникало лишних слушателей у "next" фигуры.
    /// </summary>
    public void Run()
    {
        foreach (Transform brick in transform)
        {
            if (brick.CompareTag("Brick"))
            {
                brick.GetComponent<Brick>().Init();
            }
        }
    }

    /// <summary>
    /// Переводим фигуру на турбо режим
    /// </summary>
    public void Turbo()
    {
        if(_isTurbo)
        {
            return;
        } else
        {
            _isTurbo = true;
        }
    }

}
