﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View : MonoBehaviour
{
    // панель с кнопками управления приложением
    public GameObject mainMenu;
    // кнопка перезапуска игрового уровня
    public GameObject restartButton;
    // окошко с подсказкой о повышении скорости игры
    public GameObject speedUpTip;
    // набор интерфейсных окон
    public BasePanel[] panels;
    //Текст кнопки новой игры
    public Text newGameText;

    private GameObject _backGround;
    private BasePanel _currentPanel;
    private bool _isGameOver;

    protected void Awake()
    {
        _backGround = GameObject.Find("bg_cam");
        _backGround.SetActive(false);
    }

    public void GameExit()
    {
        Debug.Log("Выходим из игры!");
        Application.Quit();
    }

    public void GameOver()
    {
        _backGround.SetActive(false);
        //hide start button
        _isGameOver = true;
        //ShowMainMenu();
        NavigationHandler(3);
    }

    public void GamePause()
    {
        ShowMainMenu();
        HidePanel(panels[0]);
        AudioManager.Instance.PlayCursor();
    }

    public void HideMainMenu()
    {
        _backGround.SetActive(true);
        mainMenu.SetActive(false);
    }

    /// <summary>
    /// Запускается по кнопке запуска игры.
    /// </summary>
    /// <param name="index">индекс панели в массиве</param>
    public void NavigationHandler(int index)
    {
        HidePanel(_currentPanel);

        //Если это кнопка перезапуска уровня или начала игры
        if (index == 0 || index == -1)
        {
            bool isRestart;
            // перезапуск уровня
            if (index == 0)
            {
                isRestart = true;
                GameManager.Instance.ResetSpeed();
            }
            // начать игру заново или продолжить
            else
            {
                if (_isGameOver)
                {
                    isRestart = true;
                    _isGameOver = false;
                }
                else
                {
                    isRestart = false;
                }
            }
            _currentPanel = UIManager.Instance.ShowOne(panels[0]);
            HideMainMenu();
            EventManager.Instance.Dispatch(GameEventType.GAME_START, isRestart);
        }
        else
        {
            _currentPanel = UIManager.Instance.ShowOne(panels[index]);
        }
        AudioManager.Instance.PlayCursor();
    }

    public void ShowAlert()
    {
        UIManager.Instance.ShowOne(panels[4]);
    }

    public void ShowDifficultyPanel()
    {
        UIManager.Instance.ShowOne(panels[6]);
    }

    public void ShowDefinedButtonPanel()
    {
        UIManager.Instance.ShowOne(panels[5]);
    }

    public void ShowMainMenu()
    {
        restartButton.SetActive(!_isGameOver);
        mainMenu.SetActive(true);
        newGameText.text = _isGameOver ? "New Game" : "Resume";
    }

    /// <summary>
    ///  Показываем окно с сообщением о повышении скорости игры
    /// </summary>
    public void ShowSpeedUpTip()
    {
        if (!speedUpTip.activeSelf)
        {
            speedUpTip.SetActive(true);
        }
    }

    /// <summary>
    /// Обновляем информацию для указанной панели
    /// </summary>
    /// <param name="panelType">числовой код панели, для которой будем обновлять данные</param>
    /// <param name="info">массив значений с данными</param>
    public void UpdatePanelInfo(int panelType, int[] info)
    {
        _currentPanel = panels[panelType];
        if (_currentPanel != null)
        {
            _currentPanel.UpdatePanelInfo(info);
        }
    }

    /// <summary>
    /// Прячем указанную панель
    /// </summary>
    /// <param name="targetPanel"></param>
    private static void HidePanel(BasePanel targetPanel)
    {
        if (targetPanel != null)
        {
            UIManager.Instance.SetClose(targetPanel);
        }
    }
}
