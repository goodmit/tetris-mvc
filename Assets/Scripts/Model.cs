﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Model : MonoBehaviour
{ 
    public const int _MAX_LINES = 21;
    public const int _MAX_COLUMNS = 10;

    public int ScoreStep = 5000;

    private Line[] _GameMap;
    private int[] _lineChecker = new int[_MAX_LINES];
    private int _counter = 0;
    private int _clearLinePoints = 0;
    private int _line { get; set; }
    private int _level { get; set; }
    private int _score { get; set; }
    private int _highScore { get; set; }

    void Awake()
    {
        LoadData();
    }

    void Start()
    {
        _GameMap = new Line[_MAX_LINES];
        InitMap(_GameMap);
    }

    #region Public Methods
    /// <summary>
    /// Проверяем, нужно ли завершать игру
    /// </summary>
    /// <returns></returns>
    public void CheckGameOver()
    {
        System.Array.Clear(_lineChecker, 0, _lineChecker.Length);
        if (!_GameMap[_MAX_LINES - 1].IsEmpty())
        {
            SendGameOver();
        }
        else
        {
            
            GameManager.Instance.ResetFigure();
        }
    }

    /// <summary>
    /// Проверка линии на заполненность.
    /// Если линия полностью заполнена кубиками - отправляем ее на удаление.
    /// </summary>
    /// <param name="line">Ряд с кубиками</param>
    public void CheckLine(int line)
    {
        _counter++;
        _lineChecker[line]++;
        if (_lineChecker[line] >= _MAX_COLUMNS)
        {
            AudioManager.Instance.PlayLineClear();
            _clearLinePoints++;
            _line++;
            RemoveLine(line);
        }
        if(_counter == Brick.BriksInAction)
        {
            _counter = 0;

            UpdateGameMap();
        }
    }

    /// <summary>
    /// Очищаем все сохраненные данные
    /// </summary>
    public void ClearData()
    {
        _highScore = 0;
        _score = 0;
        _line = 0;
        _level = 0;
        SaveData();
    }

    /// <summary>
    /// Получаем текущие значения различных параметров в игре
    /// </summary>
    /// <returns></returns>
    public int[] GetScoreInfo()
    {
        //Todo: лучше будет изменить на формат JSON
        return new[] { _highScore, _score, _line, _level };
    }

    /// <summary>
    /// Проверяем, может ли фигура находиться в этой точке карты
    /// </summary>
    /// <param name="figure">"тело" фигуры</param>
    /// <returns></returns>
    public bool IsCorrectPosition(Transform figure)
    {
        foreach (Transform brick in figure)
        {
            if (brick.CompareTag("Brick"))
            {
                Vector2 position = new Vector2(brick.position.x, brick.position.y);
                if (IsInsideMap(position) == false)
                {
                    return false;
                }

                int lineId = Mathf.RoundToInt(position.y);
                if (lineId < _MAX_LINES)
                {
                    int newX = Mathf.RoundToInt(position.x);
                    if (_GameMap[lineId].HasBrickHere(newX))
                    {
                        return false;
                    }
                }

            }
        }
        return true;
    }

    /// <summary>
    /// Фиксирует фигуру на игровой карте
    /// </summary>
    /// <param name="transform">Упавшая фигура</param>
    public void PlaceFigure(Transform transform)
    {
        GameManager.Instance.GamePause();

        int deepLine = _MAX_LINES;

        foreach (Transform brick in transform)
        {
            if (brick.CompareTag("Brick"))
            {
                Vector2 position = brick.position;
                int newX = Mathf.RoundToInt(position.x);
                int lineID = Mathf.RoundToInt(position.y);

                if(lineID >= _MAX_LINES)
                {
                    SendGameOver();
                    return;
                }

                _GameMap[lineID].AddBrick(brick);

                brick.GetComponent<Brick>().State = BrickState.FIXED;

                if (lineID < deepLine)
                {
                    deepLine = lineID;
                }
            }
        }

        EventManager.Instance.Dispatch(GameEventType.CHECK_MAP, deepLine);
    }

    /// <summary>
    /// Обновляем состояние игры до дефолтного
    /// </summary>
    public void RefreshGame()
    {
        //обновление карты и данных
        _GameMap = new Line[_MAX_LINES];
        InitMap(_GameMap);
        GameManager.Instance.GameRestart();
        _score = 0;
        _line = 0;
        _level = 0;
        EventManager.Instance.Dispatch(GameEventType.REFRESH_SCORE, 0);
    }
    #endregion

    #region Private Methods
    // сдвигаем линии вниз
    private void MoveLines(int lineIndex)
    {
        for (int i = lineIndex; i < _MAX_LINES - 2; i++)
        {
            _GameMap[i].CopyFromTopLine(_GameMap[i + 1]);
        }
    }
    
    // Инициализация игровой карты
    private void InitMap(Line[] map)
    {
        for (int i = 0; i < map.Length; i++)
        {
            map[i] = new Line(_MAX_COLUMNS);
        }
    }

    // Проверяем, находится ли кубик в пределах карты
    private bool IsInsideMap(Vector2 position)
    {
        int newX = Mathf.RoundToInt(position.x);
        int newY = Mathf.RoundToInt(position.y);
        return newX >= 0 && newX < _MAX_COLUMNS && newY >= 0;
    }

    // Загружаем данные
    private void LoadData()
    {
        _highScore = PlayerPrefs.GetInt("HighScore", 0);
    }

    // удаляем указанную линию
    private void RemoveLine(int line)
    {
        for (int i = 0; i < _MAX_COLUMNS; i++)
        {
            //Destroy(_GameMap[line].GetBrick(i).gameObject);
        }
        _GameMap[line].Clear();
    }

    // Сохраняем данные
    private void SaveData()
    {
        PlayerPrefs.SetInt("HighScore", _highScore);
    }

    // сохраняем результат и отправляем событие о завершении игры
    private void SendGameOver()
    {
        //System.Array.Clear(_lineChecker, 0, _lineChecker.Length);
        SaveData();
        EventManager.Instance.Dispatch(GameEventType.GAME_OVER);
    }

    // обновляем состояние игровой карты
    private void UpdateGameMap()
    {
        UpdateScore(_clearLinePoints);
        if (_clearLinePoints > 0)
        {
            UpdateLevelProgress();
        }
        EventManager.Instance.Dispatch(GameEventType.REFRESH_SCORE, 0);

        while (_clearLinePoints > 0)
        {
            for (int i = 0; i < _MAX_LINES - 1; i++)
            {
                if (_GameMap[i].IsEmpty())
                {
                    MoveLines(i);
                }
            }
            _clearLinePoints--;
        }
        CheckGameOver();
    }

    // Обновляем прогресс до нового уровня
    private void UpdateLevelProgress()
    {
        var tempLevel = _score / ScoreStep;
        if (tempLevel > _level)
        {
            _level = tempLevel;
            GameManager.Instance.UpgradeLevel();
        }
    }

    // обновляем количество набранных очков
    private void UpdateScore(int count)
    {
        if (count == 0)
        {
            _score += 20;
        }
        else
        {
            _score += count * 1000;
        }
        if (_score > _highScore)
        {
            _highScore = _score;
        }
    }
    #endregion
}
