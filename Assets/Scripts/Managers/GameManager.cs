﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    private const int _multiple = 20;

    [HideInInspector]
    public Figure currentFigure;
    [HideInInspector]
    public Figure nextFigure;

    public Transform blockContainer;
    public Transform nextContainer;
    public Figure[] figures;
    public Color[] colors;
    public Material[] materials;
    public int maxSpeedLevel;

    // приостановлена ли игра
    private bool _isPause = true;
    // начальная скорость падения фигур
    private float _initSpeed = 0.8f;
    // текущая скорость
    private float _currentSpeed;
    // текущий уровень
    private int _currentLevel;
    // таймер сброса времени задержки фигуры на одной линии
    private float _timer;

    protected override void Awake()
    {
        base.Awake();
        // начинаем с номинальной скорости
        _currentSpeed = _initSpeed;
        EventManager.Instance.Add(GameEventType.FIGURE_MOVE, MovementHandler);
    }

    private void OnDestroy()
    {
        EventManager.Instance.Remove(GameEventType.FIGURE_MOVE, MovementHandler);
    }

    void Update()
    {
        // если игра на паузе - ничего не предпринимаем
        if (_isPause)
        {
            return;
        }
        _timer += Time.deltaTime;
        
        // если текущей фигуры нет, то генерируем следующую
        if (currentFigure == null)
        {
            SpawnFigure();
        }

        // проверяем, нажимал ли игрок какие клавиши на клавиатуре
        InputHandler();

        // считаем время, пора ли переместить фигуру на один ряд ниже
        if (_timer > (currentFigure.IsTurbo() ? _currentSpeed / _multiple : _currentSpeed))
        {
            _timer = 0;
            currentFigure.Fall();
        }
        
    }

    #region Public Methods

    /// <summary>
    /// Ставим игру на паузу
    /// </summary>
    public void GamePause()
    {
        _isPause = true;
    }

    /// <summary>
    /// Перезапускаем игровой уровень
    /// </summary>
    public void GameRestart()
    {
        
        // сброс уровня
        _currentLevel = 0;

        for (int i = 0; i < blockContainer.childCount; i++)
        {
            var figure = blockContainer.GetChild(i).gameObject;
            Destroy(figure);
        }
        // сброс скорости
        ResetSpeed();
    }

    /// <summary>
    /// Старт игры / Снятие игры с паузы
    /// </summary>
    public void GameStart()
    {
        _isPause = false;
    }

    /// <summary>
    /// Сброс скорости игры
    /// </summary>
    public void ResetSpeed()
    {
        _currentSpeed = _initSpeed;
    }

    /// <summary>
    /// Уничтожаем ссылку на текущую фигуру
    /// </summary>
    public void ResetFigure()
    {
        currentFigure = null;
        GameStart();
    }

    /// <summary>
    /// Генерация новой фигуры на поле
    /// </summary>
    public void SpawnFigure()
    {
        // если еще есть на игровом поле активная фигура, то ничего не делаем
        if (currentFigure != null)
        {
            return;
        }
        int figureIndex;
        int materialIndex;
        int rotationOffset;
        // если зашли в игру в первый раз
        if (nextFigure == null)
        {
            rotationOffset = Random.Range(0, 4);
            figureIndex = Random.Range(0, figures.Length);
            materialIndex = Random.Range(0, materials.Length);
            currentFigure = Instantiate(figures[figureIndex]);
            currentFigure.Init(materials[materialIndex], rotationOffset);
        }
        else
        {
            currentFigure = nextFigure;
        }
        
        // инициация фигуры, которая появится следом за текущей
        rotationOffset = Random.Range(0, 4);
        figureIndex = Random.Range(0, figures.Length);
        materialIndex = Random.Range(0, materials.Length);
        nextFigure = Instantiate(figures[figureIndex]);
        nextFigure.Init(materials[materialIndex], rotationOffset);
        nextFigure.transform.parent = nextContainer;

        Camera camer = Camera.main;
        float resolution = (float)camer.pixelWidth / (float)(camer.pixelHeight);
        float cellWidth = (float)camer.pixelWidth / (float)camer.orthographicSize;

        Vector3 nextPos = nextFigure.transform.position;
        nextPos.y = -0.5f;
        nextPos.x = (nextContainer.transform.position.x / cellWidth * resolution) + 5.5f;
        nextFigure.transform.position = nextPos;
        
        // сброс положения фигуры и ее запуск
        currentFigure.transform.parent = blockContainer;
        currentFigure.transform.localPosition = Vector3.zero;
        currentFigure.transform.localScale = Vector3.one;
        currentFigure.Run();
    }

    /// <summary>
    /// Увеличиваем скорость игры
    /// </summary>
    /// <returns></returns>
    public float SpeedUp()
    {
        return _currentSpeed /= 1.5f;
    }

    /// <summary>
    /// Повышение уровня
    /// </summary>
    public void UpgradeLevel()
    {
        _currentLevel++;
        // если новый уровень превышает предельно допустимую скорость, то ничего не делаем
        if (_currentLevel > maxSpeedLevel)
        {
            return;
        }
        // увеличиваем скорость падения фигур
        _currentSpeed = SpeedUp();
        
        // отправляем уведомление игроку о том, что скорость игры увеличилась
        Controller.Instance.view.ShowSpeedUpTip();
    }

    #endregion

    #region Private Methods

    /* Обработка событий клавиатуры */
    private void InputHandler()
    {
        if(_isPause || currentFigure == null)
        {
            return;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            currentFigure.Move(true);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            currentFigure.Move();
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            currentFigure.Turbo();
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            currentFigure.Rotate(true);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentFigure.Rotate();
        }
    }

    // обработка событий от диспетчера
    private void MovementHandler(object obj = null)
    {
        if (_isPause || currentFigure == null || obj == null)
        {
            return;
        }
        switch (obj)
        {
            case Movements.DOWN:
                currentFigure.Turbo();
                break;
            case Movements.LEFT:
                currentFigure.Move(true);
                break;
            case Movements.RIGHT:
                currentFigure.Move();
                break;
            case Movements.ROTATE_LEFT:
                currentFigure.Rotate(true);
                break;
            case Movements.ROTATE_RIGHT:
                currentFigure.Rotate();
                break;
            default:
                Debug.LogError("Неверный аргумент движения!");
                break;
        }
    }
    #endregion

}
