﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    private static readonly UIStack<BasePanel> _panelStack = new UIStack<BasePanel>();

    private readonly UIMixer _panelsCompositor = new UIMixer(_panelStack);

    /// <summary>
    /// Вытянуть из стэка панель
    /// </summary>
    /// <param name="targetPanel">конкретная панель</param>
    public void SetClose(BasePanel targetPanel)
    {
        _panelsCompositor.PopPanel();
    }

    public BasePanel ShowOne(BasePanel targetPanel)
    {
        return _panelsCompositor.PushPanel(targetPanel);
    }

    /// <summary>
    /// Отображаем одну панель поверх другой
    /// </summary>
    /// <param name="targetPanel">панель для отображения</param>
    public void ShowMore(BasePanel targetPanel)
    {
        targetPanel.Show();
    }

}
