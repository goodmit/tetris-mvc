﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    // звук нажатия на кнопки
    public AudioClip click;
    // звук приземления и фиксации фигуры
    public AudioClip drop;
    // звук управления фигурой
    public AudioClip control;
    // звук удаления линии
    public AudioClip lineClear;
    // музыка в игре
    public AudioClip musicLine;

    private AudioSource[] _audioSources;
    private bool _isMute;

    protected override void Awake()
    {
        base.Awake();
        _audioSources = GetComponents<AudioSource>();
        _audioSources[0].clip = musicLine;
        Debug.Log(_audioSources);
    }

    #region Public Methods
    public void PlayControl()
    {
        PlayAudio(control, 1);
    }

    public void PlayCursor()
    {
        PlayAudio(click, 1);
    }

    public void PlayDrop()
    {
        PlayAudio(drop, 1);
    }

    public void PlayLineClear()
    {
        PlayAudio(lineClear, 1);
    }

    public void SetMusicVolume(float value)
    {
        _audioSources[0].volume = value;
    }

    public void SetSoundsVolume(float value)
    {
        _audioSources[1].volume = value;
    }

    public void SetMute(bool isMute)
    {
        _isMute = isMute;
        if (isMute == false)
        {
            PlayCursor();
        }
    }

    public void Play()
    {
        PlayMusic();
    }

    public void Pause()
    {
        StopMusic();
        /*if (_audioSources[0].isPlaying)
        {
            _audioSources[0].Pause();
        } else
        {
            _audioSources[0].Play();
        }*/
    }

    #endregion

    #region Private Methods
    private void PlayAudio(AudioClip clip, int index)
    {
        // если включен тихий режим, то игнорируем обработку
        if (_isMute) return;

        //_audioSources[index].clip = clip;
        _audioSources[index].PlayOneShot(clip);// .Play();
    }

    private void PlayMusic(AudioClip clip = null)
    {
        if (_isMute) return;
        //_audioSources[0].clip = musicLine;
        _audioSources[0].Play();
    }

    private void StopMusic()
    {
        if (_audioSources[0].isPlaying)
        {
            _audioSources[0].Stop();
        }
    }
    #endregion

}
