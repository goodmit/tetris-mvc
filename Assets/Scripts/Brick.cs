﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Состояния кубика
/// </summary>
public enum BrickState
{
    NOT_ACTIVE,
    ACTIVE,
    FIXED
}

public class Brick : MonoBehaviour
{
    public static int BriksInAction = 0;

    public BrickState State { get; set; }

    void Awake()
    {
        State = BrickState.NOT_ACTIVE;
    }

    void OnDestroy()
    {
        BriksInAction--;
        EventManager.Instance.Remove(GameEventType.CHECK_MAP, OnCheckMap);
        EventManager.Instance.Remove(GameEventType.GAME_OVER, OnKillMe);
    }
    
    public void KillMe()
    {
        Destroy(gameObject);
    }

    public void Init()
    {
        BriksInAction++;
        State = BrickState.ACTIVE;
        EventManager.Instance.Add(GameEventType.CHECK_MAP, OnCheckMap);
        EventManager.Instance.Add(GameEventType.GAME_OVER, OnKillMe);
    }

    // Кирпич самоудаляется при завершении игры
    private void OnKillMe(object obj)
    {
        KillMe();
    }

    private void OnCheckMap(object obj)
    {
        if (!(obj is int))
        {
            return;
        }

        int line = Mathf.RoundToInt(transform.position.y);
        EventManager.Instance.Dispatch(GameEventType.BRICK_IN_LINE, line);
    }
    
}
