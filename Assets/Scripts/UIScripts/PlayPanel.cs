﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayPanel : BasePanel
{
    public Text currentScoreText;
    public Text hiScoreText;
    public Text linesText;
    public Text speedText;

    public Button btnPause;
    public Button btnLeft;
    public Button btnRight;
    public Button btnDown;
    public Button btnRotateLeft;
    public Button btnRotateRight;

    void Awake()
    {
        Init();
    }

    /// <summary>
    /// Отображает панель
    /// </summary>
    /// <returns></returns>
    override public BasePanel Show()
    {
        panelType = 0;
        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
        }
        currentScoreText.text = "0";
        hiScoreText.text = "0";
        linesText.text = "0";
        speedText.text = "0";
        EventManager.Instance.Dispatch(GameEventType.REFRESH_SCORE, Convert.ToInt32(panelType));
        return this;
    }

    /// <summary>
    /// Инициализирует панель (включает кнопки, загружает данные)
    /// </summary>
    override public void Init()
    {
        panelType = 0;
        
        btnLeft.onClick.AddListener(() => EventManager.Instance.Dispatch(GameEventType.FIGURE_MOVE, Movements.LEFT));
        btnRight.onClick.AddListener(() => EventManager.Instance.Dispatch(GameEventType.FIGURE_MOVE, Movements.RIGHT));
        btnRotateLeft.onClick.AddListener(() => EventManager.Instance.Dispatch(GameEventType.FIGURE_MOVE, Movements.ROTATE_LEFT));
        btnRotateRight.onClick.AddListener(() => EventManager.Instance.Dispatch(GameEventType.FIGURE_MOVE, Movements.ROTATE_RIGHT));
        btnDown.onClick.AddListener(() => EventManager.Instance.Dispatch(GameEventType.FIGURE_MOVE, Movements.DOWN));
        btnPause.onClick.AddListener(() => EventManager.Instance.Dispatch(GameEventType.GAME_PAUSE, true));
    }

    /// <summary>
    /// Скрывает панель
    /// </summary>
    override public void Hide()
    {
        if (gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Удаляет панель из стэка
    /// </summary>
    override public void Destroy()
    {

    }

    /// <summary>
    /// Обновляем в оверлее игровые параметры (очки, рекорды, скорость, количество удаленных линии)
    /// </summary>
    /// <param name="info">массив с данными [hiscore, score, lines, level]</param>
    override public void UpdatePanelInfo(int[] info)
    {
        
        if (info == null || info.Length == 0)
        {
            return;
        }

        hiScoreText.text = info[0].ToString();
        currentScoreText.text = info[1].ToString();
        linesText.text = info[2].ToString();
        speedText.text = info[3].ToString();

    }
}
