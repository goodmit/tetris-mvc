﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankPanel : BasePanel
{
    public Button clearButton;
    public Button closeButton;

    public Text hiScoreTxt;
    
    void Awake()
    {
        Init();
    }

    /// <summary>
    /// Отображает панель
    /// </summary>
    /// <returns></returns>
    override public BasePanel Show()
    {
        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
        }
        EventManager.Instance.Dispatch(GameEventType.REFRESH_SCORE, Convert.ToInt32(panelType));
        return this;
    }

    /// <summary>
    /// Инициализирует панель (включает кнопки, загружает данные)
    /// </summary>
    override public void Init()
    {
        panelType = 2;
        clearButton.onClick.AddListener(() => 
        {
            EventManager.Instance.Dispatch(GameEventType.CLEAR_DATA, Convert.ToInt32(panelType));
            AudioManager.Instance.PlayCursor();
        });
        //clearButton.onClick.AddListener(() => AudioManager.Instance.PlayCursor());
        closeButton.onClick.AddListener(() => EventManager.Instance.Dispatch(GameEventType.GAME_PAUSE, false));
    }

    /// <summary>
    /// Скрывает панель
    /// </summary>
    override public void Hide()
    {
        hiScoreTxt.text = "";
        if (gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Обновляет инфо о панели
    /// </summary>
    /// <param name="info"></param>
    public override void UpdatePanelInfo(int[] info)
    {
        if (info == null || info.Length == 0)
        {
            return;
        }
        hiScoreTxt.text = info[0].ToString();
    }

    /// <summary>
    /// Удаляет панель из стэка
    /// </summary>
    override public void Destroy()
    {

    }

}
