﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPanel : BasePanel
{
    public Text currentScoreText;
    public Text highestScoreText;
    public Text titleText;
    public Button closeButton;

    void Awake()
    {
        Init();
    }

    /// <summary>
    /// Отображает панель
    /// </summary>
    /// <returns></returns>
    override public BasePanel Show()
    {
        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
        }
        EventManager.Instance.Dispatch(GameEventType.REFRESH_SCORE, Convert.ToInt32(panelType));
        return this;
    }

    /// <summary>
    /// Инициализирует панель (включает кнопки, загружает данные)
    /// </summary>
    override public void Init()
    {
        panelType = 3;
        closeButton.onClick.AddListener(() => {
            EventManager.Instance.Dispatch(GameEventType.GAME_PAUSE, false);
            AudioManager.Instance.Pause();
        });
    }

    /// <summary>
    /// Скрывает панель
    /// </summary>
    override public void Hide()
    {
        highestScoreText.text = "";
        currentScoreText.text = "";
        titleText.text = "";
        if (gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        }

    }

    /// <summary>
    /// Удаляет панель из стэка
    /// </summary>
    override public void Destroy()
    {

    }

    /// <summary>
    /// Обновляет инфо о панели
    /// </summary>
    /// <param name="info"></param>
    override public void UpdatePanelInfo(int[] info)
    {
        if (info == null || info.Length == 0)
        {
            return;
        }
        string label = info[0] <= info[1] ? "New Record!" : "Game Over!";
        //highestScoreText.text = info[0] > info[1] ? info[0].ToString() : "";
        //currentScoreText.text = info[0] <= info[1] ? info[1].ToString() : "";
        highestScoreText.text = "";
        currentScoreText.text = info[1].ToString();
        titleText.text = label;
    }

}
