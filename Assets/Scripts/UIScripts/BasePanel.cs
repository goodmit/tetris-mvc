﻿using UnityEngine;

public abstract class BasePanel : MonoBehaviour
{
    /// <summary>
    /// Код типа панели
    /// </summary>
    [HideInInspector]
    public int panelType;

    /// <summary>
    /// Становится true, когда в стэк добавляется панель
    /// </summary>
    [HideInInspector]
    public bool stack = false;

    /// <summary>
    /// Отображаем панель
    /// </summary>
    /// <returns></returns>
    public abstract BasePanel Show();

    /// <summary>
    /// Обновляем информацию о панели
    /// </summary>
    public virtual void UpdatePanelInfo(int[] info) { }

    /// <summary>
    /// Инициализируем панель (включает кнопки, загружает данные)
    /// </summary>
    public abstract void Init();

    /// <summary>
    /// Скрываем панель
    /// </summary>
    public abstract void Hide();

    /// <summary>
    /// Удаляем панель из стэка
    /// </summary>
    public abstract void Destroy();
}
