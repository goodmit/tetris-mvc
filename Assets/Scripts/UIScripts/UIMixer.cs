﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMixer// : MonoBehaviour
{
    private readonly UIStack<BasePanel> _panelStack;

    public UIMixer(UIStack<BasePanel> panelStack)
    {
        _panelStack = panelStack;
    }

    /// <summary>
    /// Добавить панель
    /// </summary>
    /// <param name="targetPanel"></param>
    /// <returns></returns>
    public BasePanel PushPanel(BasePanel targetPanel)
    {
        if (targetPanel == _panelStack.Peek())
        {
            return targetPanel;
        }
        _panelStack.Remove(targetPanel);
        var tempPanel = _panelStack.Peek();
        if (tempPanel != null)
        {
            //Если новое окно закрыто, его не нужно снова отображать
            if (!tempPanel.stack)
            {
                _panelStack.Pop();
            }
            //Независимо от того, существует ли всплывающий стек, его нужно скрыть
            tempPanel.Hide();
        }
        targetPanel.Show();
        return _panelStack.Push(targetPanel);
    }

    public void PopPanel()
    {
        _panelStack.Pop()?.Hide();
        _panelStack.Peek()?.Show();
    }

    public void ClearAllPanel()
    {
        _panelStack.Peek()?.Hide();
        _panelStack.Clear();
    }

}
