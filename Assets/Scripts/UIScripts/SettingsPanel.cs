﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPanel : BasePanel
{
    public Button closeButton;
    public Slider musicVolume;
    public Slider soundsVolume;
    public Toggle muteToggle;

    void Awake()
    {
        Init();
    }

    /// <summary>
    /// Отображаем панель
    /// </summary>
    /// <returns></returns>
    override public BasePanel Show()
    {
        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
        }
        return this;
    }

    /// <summary>
    /// Инициализируем панель (включает кнопки, загружает данные)
    /// </summary>
    override public void Init()
    {
        panelType = 1;
        stack = true;
        closeButton.onClick.AddListener(() => 
        {
            EventManager.Instance.Dispatch(GameEventType.VOLUME_SOUND, soundsVolume.normalizedValue);
            EventManager.Instance.Dispatch(GameEventType.VOLUME_MUSIC, musicVolume.normalizedValue);
            EventManager.Instance.Dispatch(GameEventType.GAME_PAUSE, false);
        });
    }

    /// <summary>
    /// Скрываем панель
    /// </summary>
    override public void Hide()
    {
        if (gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Удаляем панель из стэка
    /// </summary>
    override public void Destroy()
    {

    }

    /// <summary>
    /// Включаем/выключаем бесшумный режим
    /// </summary>
    public void MuteIt()
    {
        EventManager.Instance.Dispatch(GameEventType.SET_MUTE, muteToggle.isOn);
    }

    // диспатчим изменения для громкости звуков
    private void onSoundVolumeChanged(float value)
    {
        EventManager.Instance.Dispatch(GameEventType.VOLUME_SOUND, value);
    }

    // диспатчим изменения для громкости музыки
    private void onMusicVolumeChanged(float value)
    {
        EventManager.Instance.Dispatch(GameEventType.VOLUME_MUSIC, value);
    }


}
