﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIStack<T> : IEnumerable where T : MonoBehaviour
{
    // размер стэка по умолчанию
    private const int _DEFAULT_STACK_SIZE = 5;
    // массив, в котором будут размещаться элементы
    private T[] _stackArray;

    public int Size { get; private set; }

    public UIStack()
    {
        _stackArray = new T[_DEFAULT_STACK_SIZE];
        Size = 0;
    }

    public void Clear()
    {
        Size = 0;
        ArrayGC(ref _stackArray, Size);
    }

    /// <summary>
    /// Возвращает все элементы в стеке, один за другим
    /// </summary>
    /// <returns></returns>
    public IEnumerator GetEnumerator()
    {
        for (int i = Size - 1; i >= 0; i--)
        {
            yield return _stackArray[i];
        }
    }

    public T Peek()
    {
        return (Size == 0) ? default : _stackArray[Size - 1];
    }

    public T Pop()
    {
        if (Size == 0)
        {
            return default;
        }

        Size--;
        var result = _stackArray[Size];
        ArrayGC(ref _stackArray, Size);
        return result;
    }

    public T Push(T item)
    {
        if (Equals(item, default(T)))
        {
            return item;
        }
        _stackArray[Size++] = item;
        if (Size >= _stackArray.Length - 1)
        {
            Resize();
        }
        return default;
    }

    public bool Remove(T item)
    {
        if (Equals(item, default(T)))
        {
            return false;
        }

        var isFound = false;
        for (int i = 0; i < Size; i++)
        {
            if (!isFound && Equals(item, _stackArray[i]))
            {
                isFound = true;
            }
            if (isFound)
            {
                //if end
                if (i + 1 == Size)
                {
                    Size--;
                    ArrayGC(ref _stackArray, Size);
                    return true;
                }
                //if not end, copy
                _stackArray[i] = _stackArray[i + 1];
            }
        }
        return isFound;
    }

    private void Resize()
    {
        var newArray = new T[_stackArray.Length * 2];
        for (int i = 0; i < _stackArray.Length; i++)
        {
            newArray[i] = _stackArray[i];
        }
        _stackArray = newArray;
    }

    private void ArrayGC(ref T[] array, int index)
    {
        for (int i = index; i < array.Length; i++)
        {
            if (Equals(default(T), array[i]))
            {
                return;
            }
            array[i] = default;
        }
    }
}
