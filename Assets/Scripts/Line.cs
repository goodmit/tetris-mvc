﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line
{
    private Transform[] _line;

    #region Public Methods

    /// <summary>
    /// Конструктор.
    /// Принимает один параметр - количество столбцов.
    /// </summary>
    /// <param name="rows">Размер игрового ряда (количество столбцов)</param>
    public Line(int rows)
    {
        _line = new Transform[rows];
    }
    
    /// <summary>
    /// Добавляем кубик в ряд
    /// </summary>
    /// <param name="brick">кубик</param>
    public void AddBrick(Transform brick)
    {
        int posX = Mathf.RoundToInt(brick.transform.position.x);
        _line[posX] = brick;
    }

    /// <summary>
    /// Очистить ряд
    /// </summary>
    public void Clear(bool dispose = true)
    {
        for (int i = 0; i < _line.Length; i++)
        {
            if (_line[i] == null)
            {
                continue;
            }
            if (dispose)
            {
                _line[i].GetComponent<Brick>().KillMe();
            }
            _line[i] = null;
        }
    }

    /// <summary>
    /// Копируем текущий ряд с верхнего ряда
    /// </summary>
    /// <param name="line">ряд, который находится выше текущего</param>
    public void CopyFromTopLine(Line line)
    {
        for (int i = 0; i < _line.Length; i++)
        {
            if (line.HasBrickHere(i))
            {   
                _line[i] = line.GetBrick(i);
                _line[i].position += Vector3.down;
            }
        }
        line.Clear(false);
    }

    /// <summary>
    /// Получить кубик по номеру столбца
    /// </summary>
    /// <param name="i"></param>
    /// <returns></returns>
    public Transform GetBrick(int i)
    {
        return _line[i];
    }

    /// <summary>
    /// Проверяем, есть ли кубик в конкретном столбце
    /// </summary>
    /// <param name="_x"></param>
    /// <returns></returns>
    public bool HasBrickHere(int _x)
    {
        return (_line[_x] != null);
    }

    /// <summary>
    /// Проверяем, пустой ли ряд
    /// </summary>
    /// <returns></returns>
    public bool IsEmpty()
    {
        for (int i = 0; i < _line.Length; i++)
        {
            if(_line[i] != null)
            {
                return false;
            }
        }
        return true;
    }
    #endregion

}
